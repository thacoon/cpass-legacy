#include "test_settings.h"


TEST create_and_read_json_string() {
    char *service_name = "gitlab";
    size_t password_length = 20;
    char *alphabet = "simple";
    char *created_on = "2018-05-01";

    service s;
    s.name = service_name;
    s.password_length = password_length;
    s.alphabet = alphabet;
    s.created_on = created_on;

    cJSON *actual_json = set_create_json(s);
    service actual_s = set_create_service_from_json(actual_json);

    ASSERT_EQ(true, actual_s.ok);
    ASSERT_STR_EQ(service_name, actual_s.name);
    ASSERT_EQ(password_length, actual_s.password_length);
    ASSERT_STR_EQ(alphabet, actual_s.alphabet);
    ASSERT_STR_EQ(created_on, actual_s.created_on);

    set_free_single_service(&actual_s);
    cJSON_Delete(actual_json);
    PASS();
}

GREATEST_SUITE(settings_suite) {
    RUN_TEST(create_and_read_json_string);
}
