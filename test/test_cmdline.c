#include "test_cmdline.h"

DEFINE_FFF_GLOBALS

FAKE_VALUE_FUNC(char *, fgets, char *, int, FILE *);

char *fgets_mock_password(char *s, int size, FILE *stream) {
    const char *buf = "my password\n";

    strncpy(s, buf, strlen(buf)+1);

    return s;
}

char *fgets_mock_password_no_null_terminator(char *s, int size, FILE *stream) {
    const char *buf = "my password\n";

    strncpy(s, buf, strlen(buf));  // this will not copy the null termiantor

    return s;
}

char *fgets_mock_password_return_null(char *s, int size, FILE *stream) {
    return NULL;
}

char *fgets_mock_service(char *str, int n, FILE *stream) {
    strcpy(str, "my service\n");
    return str;
}

char *fgets_mock_service_no_newline(char *str, int n, FILE *stream) {
    strcpy(str, "my service");
    return str;
}

TEST test_ask_user_for_password() {
    fgets_fake.custom_fake = fgets_mock_password;

    char *buf_returned = cmd_ask_user_for_password();

    ASSERT_EQ(1, fgets_fake.call_count);
    ASSERT_STR_EQ("my password", buf_returned);

    sodium_free(buf_returned);

    PASS();
}

TEST test_ask_user_for_password_with_no_terminator() {
    fgets_fake.custom_fake = fgets_mock_password_no_null_terminator;

    char *buf_returned = cmd_ask_user_for_password();

    ASSERT_EQ(1, fgets_fake.call_count);
    ASSERT_STR_EQ("my password", buf_returned);  // the string was returned by fgets_mock without a \0 char

    sodium_free(buf_returned);

    PASS();
}

TEST test_ask_user_for_password_with_fgets_returning_NULL() {
    fgets_fake.custom_fake = fgets_mock_password_return_null;

    char *buf_returned = cmd_ask_user_for_password();

    ASSERT_EQ(1, fgets_fake.call_count);
    ASSERT_STR_EQ("", buf_returned);

    sodium_free(buf_returned);

    PASS();
}

TEST test_ask_user_for_servcie() {
    char buffer[25];
    fgets_fake.custom_fake = fgets_mock_service;

    cmd_ask_user_for_service(buffer, 25);

    ASSERT_STR_EQ("my service", buffer);
    PASS();
}

// There was a bug causing segmention fault if there is no '\n' char at the end
TEST test_ask_user_for_servcie_with_no_newline_character() {
    char buffer[25];
    fgets_fake.custom_fake = fgets_mock_service_no_newline;

    cmd_ask_user_for_service(buffer, 25);

    ASSERT_STR_EQ("my service", buffer);
    PASS();
}

TEST test_print_passphrase() {
    char buf[BUFSIZ];
    memset(buf,'\0',BUFSIZ);
    freopen("/dev/null", "a", stdout);

    setbuf(stdout, buf);

    char password[25] = "my password";
    char service_name[25] = "my service";
    cmd_print_passphrase(password, service_name);

    freopen("/dev/tty", "a", stdout);

    ASSERT_STR_EQ("Password: QM77caxMzAczAywAZWud\n", buf);
    PASS();
}

TEST test_if_passwords_are_equal_return_zero() {
    const char *password_one = "the same password";
    const char *password_two = "the same password";

    int return_value = cmd_compare_passwords(password_one, password_two);

    ASSERT_EQ(0, return_value);
    PASS();
}

TEST test_if_passwords_are_not_equal_return_minus_one() {
    const char *password_one = "the same password";
    const char *password_two = "not the same password";

    int return_value = cmd_compare_passwords(password_one, password_two);

    ASSERT_EQ(-1, return_value);
    PASS();
}

TEST test_get_longest_service_name() {
    service service_ary[3];

    service_ary[0].name = "i am short";
    service_ary[1].name = "i am the longest";
    service_ary[2].name = "i dont care";

    size_t actual_length = cmd_get_longest_service_name(service_ary, 3);

    ASSERT_EQ(strlen("i am the longest"), actual_length);
    PASS();
}

void setup_function_cmdline(void *param) {
    RESET_FAKE(fgets);
}

void teardown_function_cmdline(void *param) {
}

GREATEST_SUITE(cmdline_suite) {
    cmd_init();

    SET_SETUP(&setup_function_cmdline, cmdline_suite);
    SET_TEARDOWN(&teardown_function_cmdline, cmdline_suite);

    RUN_TEST(test_ask_user_for_password);
    RUN_TEST(test_ask_user_for_password_with_no_terminator);
    RUN_TEST(test_ask_user_for_password_with_fgets_returning_NULL);
    RUN_TEST(test_ask_user_for_servcie);
    RUN_TEST(test_ask_user_for_servcie_with_no_newline_character);
    RUN_TEST(test_print_passphrase);
    RUN_TEST(test_if_passwords_are_equal_return_zero);
    RUN_TEST(test_if_passwords_are_not_equal_return_minus_one);
    RUN_TEST(test_get_longest_service_name);
}
