#include "test_cryp.h"

const char *password_one = "my password";
const char *password_one_hash = "e28bdbf8faa97dab2203fcc89e397a4bf8d4a5b370421e5481a55f317caee4f81be5a810bb1cffc4695c32198717b9a6e835895852ee3a8689d0963463f2db15";  //hash of "my password"

const char * const password_qwerty = "qwerty";
unsigned long long password_qwerty_length = 6;
const unsigned char *password_qwerty_hash_hex = (unsigned char *)"\x0d\xd3\xe5\x12\x64\x2c\x97\xca\x3f\x74\x7f\x9a\x76\xe3\x74\xfb\xda\x73\xf9\x29\x28\x23\xc0\x31\x3b\xe9\xd7\x8a\xdd\x7c\xdd\x8f\x72\x23\x5a\xf0\xc5\x53\xdd\x26\x79\x7e\x78\xe1\x85\x4e\xde\xe0\xae\x00\x2f\x8a\xba\x07\x4b\x06\x6d\xfc\xe1\xaf\x11\x4e\x32\xf8";
const char *password_qwerty_hash = "0dd3e512642c97ca3f747f9a76e374fbda73f9292823c0313be9d78add7cdd8f72235af0c553dd26797e78e1854edee0ae002f8aba074b066dfce1af114e32f8";

TEST test_generate_sha512_hash_from_password_qwerty() {
    unsigned char *hash = cryp_sha512(password_qwerty);

    ASSERT(hash != NULL);
    ASSERT_STR_EQ(password_qwerty_hash_hex, hash);

    sodium_free(hash);

    PASS();
}

TEST test_generate_sha512_hex_hash_and_convert_to_char_string_from_password_qwerty() {
    unsigned char *hash_hex = cryp_sha512(password_qwerty);
    ASSERT(hash_hex != NULL);

    char *hash = cryp_convert_sha512_hex_to_char(hash_hex);

    ASSERT_STR_EQ(password_qwerty_hash, hash);

    sodium_free(hash_hex);
    sodium_free(hash);

    PASS();
}

TEST test_generate_sha512_hex_hash_and_convert_to_char_string_from_password_one() {
    unsigned char *hash_hex = cryp_sha512(password_one);
    ASSERT(hash_hex != NULL);

    char *hash = cryp_convert_sha512_hex_to_char(hash_hex);

    ASSERT_STR_EQ(password_one_hash, hash);

    sodium_free(hash_hex);
    sodium_free(hash);

    PASS();
}

TEST test_derive_passphrase_from_sha512_hash() {
    unsigned char *hash = cryp_sha512("my password");
    ASSERT(hash != NULL);

    char *password = cryp_derive_passphrase(hash, 20);
    ASSERT(password != NULL);

    ASSERT_STR_EQ("50TwKk37QzIAudNLYROo", password);

    sodium_free(hash);
    sodium_free(password);

    PASS();
}

void setup_function_crypto(void *param) {
}

void teardown_function_crypto(void *param) {
}


GREATEST_SUITE(crypto_suite) {
    cryp_init();

    SET_SETUP(&setup_function_crypto, crypto_suite);
    SET_TEARDOWN(&teardown_function_crypto, crypto_suite);

    RUN_TEST(test_generate_sha512_hash_from_password_qwerty);
    RUN_TEST(test_generate_sha512_hex_hash_and_convert_to_char_string_from_password_qwerty);
    RUN_TEST(test_generate_sha512_hex_hash_and_convert_to_char_string_from_password_one);
    RUN_TEST(test_derive_passphrase_from_sha512_hash);
}
