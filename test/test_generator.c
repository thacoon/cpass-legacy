#include "test_generator.h"

TEST return_correct_sha512_hash() {
    const char *expected_hash = "e28bdbf8faa97dab2203fcc89e397a4bf8d4a5b370421e5481a55f317caee4f81be5a810bb1cffc4695c32198717b9a6e835895852ee3a8689d0963463f2db15";  //hash of "my password"
    const char *password = "my password";
    char *hash_ptr = gen_sha512(password);

    ASSERT_STR_EQ(expected_hash, hash_ptr);

    sodium_free(hash_ptr);
    PASS();
}

TEST return_correct_password() {
    const char * expected_password = "50TwKk37QzIAudNLYROo";  //password for "my password"
    char password[21];

    char *hash_ptr = gen_sha512("my password");
    gen_password(hash_ptr, password);

    ASSERT_STR_EQ(expected_password, password);

    sodium_free(hash_ptr);
    PASS();
}

TEST generate_correct_password() {
    const char *expected_password = "SwieLZdmewRnpVQ7ahhF";  //password for "my second password"

    char *password = gen_generate_password("my second password");

    ASSERT_STR_EQ(expected_password, password);

    sodium_free(password);
    PASS();
}

GREATEST_SUITE(generator_suite) {
    gen_init();

    RUN_TEST(return_correct_sha512_hash);
    RUN_TEST(return_correct_password);
    RUN_TEST(generate_correct_password);
}
