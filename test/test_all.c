#include "lib/greatest.h"

#include "test_cmdline.h"
#include "test_cryp.h"
#include "test_generator.h"
#include "test_settings.h"

extern SUITE(crypto_suite);
extern SUITE(cmdline_suite);
extern SUITE(generator_suite);
extern SUITE(settings_suite);

/* Expand to all the definitions that need to be in
   the test runner's main file. */
GREATEST_MAIN_DEFS();

int main(int argc, char **argv) {
    GREATEST_MAIN_BEGIN();      /* command-line arguments, initialization. */

    RUN_SUITE(crypto_suite);
    RUN_SUITE(cmdline_suite);
    RUN_SUITE(generator_suite);
    RUN_SUITE(settings_suite);

    GREATEST_MAIN_END();        /* display results */
 }
