cpass
=====
[![pipeline status](https://gitlab.com/thacoon/cpass/badges/master/pipeline.svg)](https://gitlab.com/thacoon/cpass/commits/master)

This is a simple terminal password manager creating passwords based on a single master password and unique service names defined by you for everything you need a password for.
You only need to remember your master password. The generated passwords are not stored anywhere. 
That means even if you lose all your data you can still clone this repository again and generate your valid passwords, if you can remember your master password and the service names.

## Requirements

```bash
$ sudo pacman -S cmake make gcc gmp openssl libsodium
```

## Compilation
```bash
$ git clone git@gitlab.com:thacoon/cpass.git
$ cd cpass
$ cmake CMakeLists.txt
# Compile and run unit tests
$ make cpass_test
$ bin/cpass_test
# Compile cpass
$ make
```

## Usage

```bash
$  bin/cpass
Welcome to cpass - that wants to become a simple passwordmanager someday.
0 - Show all used services.
1 - Generate a password.
2 - Generate a password for a new service.
3 - Exit.
> 0
id |            name | password length |        alphabet |      created on
--------------------------------------------------------------------------
00 |     gitlab-2018 |              20 |          simple |      2018-05-10
> 2
Enter your master password: 
Enter your master password: 
Enter the service: github-2018
Password: 8ByuSI2hjjVHK5FAcp64
> 1
Enter your master password: 
Enter the service: gitlab-2018
Password: hO5QA2rRYtrw79lfHraJ
> 3
Exit.
```

Cpass will create a settings file under the following path */home/$USERNAME/.cpass/settings.json*. It looks loke the following.
```bash
{
	"services_path":	"/home/username/.cpass/services.json"
}
```
The settings file will always be created there. But you can change the path of the services json file. This file holds the names of the services for that you have generated a new password (menu point 2).
To have your service json file in your Cloud, you can just change the path.
```bash
{
	"services_path":	"/home/username/Cloud/.cpass/services.json"
}
```

## Tests
```bash
$ make cpass_test
$ # Run unit tests
$ bin/cpass_test
$ # Run unit tests with valgrind
$ valgrind --track-origins=yes --leak-check=full bin/cpass_test
```

## TODO
- Cryp
    - [ ] Support different alphabets
    - [ ] Support different password lengths
    - [ ] Support encryption of *services.json* file
- Operating Systems
    - [ ] Add ArchLinux AUR package
- Settings
    - [ ] Add additional custom information for a specific service, e.g. username
    - [ ] Change alphabet
    - [ ] Change *services.json* path
    - [ ] Change custom information for a specific service
    - [ ] Change password length
    - [ ] Change service name
    - [ ] Delete unneeded service
    - [ ] Encrypted *service.json* file
    - [ ] Support different alphabets
    - [ ] Support different password lengths
    - [x] Support different *services.json* path
    - [x] Save new services
- Tests
    - [ ] Add code coverage
    - [ ] Basic fuzzing
    - [ ] Basic functional tests
    - [ ] Support different *services.json* for testing purpose
