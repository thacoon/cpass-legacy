#include "cryp.h"

const char *CRYP_ALPHABET_SIMPLE = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

#if defined(__linux__) && defined(RNDGETENTCNT)
void cryp_check_entropy(void) {
    int fd, c;

    if ((fd = open("/dev/random", O_RDONLY)) != -1) {
        if (ioctl(fd, RNDGETENTCNT, &c) == 0 && c < 160) {
            fprintf(stderr, "crypto_check_entropy: Failed, this system doesn't provide enough entropy to quickly generate high-quality random numbers.\n"
            "Installing the rng-utils/rng-tools, jitterentropy or haveged packages may help.\n");
            abort();
        }
        close(fd);
    }
}
#else
void cryp_check_entropy(void) {
    return;
}
#endif // crypto_check_entropy

int cryp_init() {
    cryp_check_entropy();

    if(sodium_init() == -1) {
        fprintf(stderr, "crypto_setup: Failed to initialize libsodium.\n");
        return -1;
    }

    return 0;
}

unsigned char *cryp_sha512(const char *str) {
    unsigned char *hash = (unsigned char *) sodium_allocarray(crypto_hash_sha512_BYTES, sizeof(unsigned char));

    unsigned long long str_len = (unsigned long long) strlen(str);
    crypto_hash_sha512(hash, (const unsigned char *) str, str_len);

    return hash;
}

char *cryp_convert_sha512_hex_to_char(const unsigned char *hash) {
    // one hex value is stored in two chars, so we need twice as much memory
    size_t hash_as_chars_len = HASH_SHA512_LEN;
    char *hash_as_chars = (char *) sodium_allocarray((HASH_SHA512_LEN_WITH_NULLBYTE), sizeof(char));
    sodium_memzero(hash_as_chars, HASH_SHA512_LEN_WITH_NULLBYTE);

    int i, j;
    for(i = 0, j = 0; (i < hash_as_chars_len) && (j < crypto_hash_sha512_BYTES); i += 2, ++j) {
        // 3 because one is reserved for '\0' so one hex is converted to 2 chars, the null character will then always be overwitten
        snprintf(hash_as_chars + i, 3, "%02x", *(hash + j));
    }
    hash_as_chars[hash_as_chars_len] = '\0';

    return hash_as_chars;
}

char *cryp_derive_passphrase(unsigned char *hash, size_t passphrase_len) {
    mpz_t num, num_idx;

    mpz_init(num);
    mpz_init(num_idx);

    char *passphrase = NULL;

    // hash is stored in hex but gnu gmp needs a char array
    char *hash_as_chars = cryp_convert_sha512_hex_to_char(hash);

    if(mpz_set_str(num, (const char *) hash_as_chars, 16) != 0) {
        fprintf(stderr, "cryp_derive_passphrase: Failed to assign hash value to integer.\n");
        goto cleanup;
    }

    passphrase = (char *) sodium_allocarray((passphrase_len + 1), sizeof(passphrase));

    int num_chars = strlen(CRYP_ALPHABET_SIMPLE);
    unsigned long long idx;

    size_t i;
    for(i = 0; i < passphrase_len; ++i) {
        mpz_mod_ui(num_idx, num, num_chars);
        mpz_div_ui(num, num, num_chars);
        idx = mpz_get_ui(num_idx);

        passphrase[i] = CRYP_ALPHABET_SIMPLE[idx];
    }
    passphrase[i] = '\0';

    cleanup:
        sodium_memzero(hash_as_chars, HASH_SHA512_LEN_WITH_NULLBYTE);
        sodium_free(hash_as_chars);

        mpz_clear(num);
        mpz_clear(num_idx);

    return passphrase;
}
