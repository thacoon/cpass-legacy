#include <stdio.h>
#include <unistd.h>

#include "cmdline.h"
#include "generator.h"
#include "settings.h"

enum menu_point{SHOW, GENERATE, NEW, EXIT} menu_point_choosed;

int main(void) {
    printf("Welcome to cpass - that wants to become a simple passwordmanager someday.\n");

    printf("%d - Show all used services.\n", SHOW);
    printf("%d - Generate a password.\n", GENERATE);
    printf("%d - Generate a password for a new service.\n", NEW);
    printf("%d - Exit.\n", EXIT);

    #if defined(__linux__) && defined (RNDGETENTCNT)
        cryp_check_entropy();
    #endif // only on Linux systems

    // Init
    set_init();
    cmd_init();
    gen_init();

    // menu
    int choosed = 0;
    char *buffer_password = NULL;  // only allocate memory with sodium_allocarray
    char *buffer_password_verify = NULL;  // only allocate memory with sodium_allocarray
    char buffer_service[512];

    while(choosed != EXIT) {
        char check_newline = 0;

        printf("> ");
        scanf("%d%c", &choosed, &check_newline);

        if(check_newline != '\n') { // wrong input
            choosed = -1;

            while(fgetc(stdin) != '\n') {
                // read until newline is found
            }
        }

        menu_point_choosed = choosed;
        switch(menu_point_choosed) {
            case SHOW:
                cmd_print_all_services();
                break;

            case GENERATE:
                buffer_password = cmd_ask_user_for_password();
                cmd_ask_user_for_service(buffer_service, 512);
                cmd_print_passphrase(buffer_password, buffer_service);
                sodium_free(buffer_password);
                break;

            case NEW:
                buffer_password = cmd_ask_user_for_password();
                buffer_password_verify = cmd_ask_user_for_password();
                if(cmd_compare_passwords(buffer_password, buffer_password_verify) == 0) {
                    cmd_ask_user_for_service(buffer_service, 256);
                    cmd_save_new_service(buffer_service);
                    cmd_print_passphrase(buffer_password, buffer_service);
                }
                else {
                    printf("Passwords do not match.\n");
                }

                sodium_free(buffer_password);
                sodium_free(buffer_password_verify);
                break;

            case EXIT:
                printf("Exit.\n");
                break;
                
            default:
                printf("Wrong input.\n");
                break;
        }
    }

    return 0;
}
