#include "settings.h"

void set_init() {
    char *settings_dir_path = set_get_settings_dir_path();
    char *settings_file_path = set_get_settings_file_path();

    struct stat st = {0};
    if(stat(settings_dir_path, &st) < 0) {
        // dir does not exist

        if(mkdir(settings_dir_path, 0700) < 0) {
            fprintf(stderr, "Error: set_init failed on create default settings dir.\n");

            goto cleanup;
        }
    }

    if(access(settings_file_path, F_OK) < 0) {
        // settings file does not exist
        char *services_path = set_get_services_file_path();

        cJSON *path = cJSON_CreateString(services_path);
        cJSON *json = cJSON_CreateObject();
        cJSON_AddItemToObject(json, "services_path", path);

        char *json_string = cJSON_Print(json);
        set_write_file(settings_file_path, json_string);

        cJSON_Delete(json);
        free(json_string);
        free(services_path);
    }

    cleanup:
        free(settings_dir_path);
        free(settings_file_path);
}

char *set_get_settings_dir_path(void) {
    const char home_path[] = "/home/";
    const char dir_name[] = "/.cpass";

    const char *username = secure_getenv("USERNAME");
    if(username == NULL) {
        fprintf(stderr, "Error: set_get_settings_dir_path failed on reading USERNAME environment variable.\n");
        return NULL;
    }

    size_t path_size = strlen(home_path) + strlen(username) + strlen(dir_name) + 1;

    char *dir_path = (char *) malloc(path_size * sizeof(char));
    if(dir_path == NULL) {
        fprintf(stderr, "Error: set_get_settings_dir_path failed on allocating memory.\n");
        return NULL;
    }
    else {
        dir_path[0] = '\0';
    }

    // cat the path of the default /home/$USERNAME/.cpass directory
    strncat(dir_path, home_path, path_size);
    strncat(dir_path, username, path_size);
    strncat(dir_path, dir_name, path_size);

    return dir_path;
}

char *set_get_settings_file_path(void) {
    const char file_name[] = "/settings.json";

    char *dir_path = set_get_settings_dir_path();

    size_t path_size = strlen(dir_path) + strlen(file_name) + 1;

    char *file_path = (char *) malloc(path_size * sizeof(char));
    file_path[0] = '\0';

    // cat the path of the default /home/$USERNAME/.cpass/settings.json file
    strncat(file_path, dir_path, path_size);
    strncat(file_path, file_name, path_size);

    free(dir_path);

    return file_path;
}

char *set_get_services_file_path(void) {
    char *settings_file_path = set_get_settings_file_path();
    char *data = set_read_file(settings_file_path);
    cJSON *settings = cJSON_Parse(data);

    char *file_path = NULL;

    cJSON *services_path = cJSON_GetObjectItem(settings, "services_path");
    if(cJSON_IsString(services_path) && (services_path->valuestring != NULL)) {
        file_path = (char *) malloc((strlen(services_path->valuestring) + 1) * sizeof(char));

        if(file_path == NULL) {
            fprintf(stderr, "Error: set_get_services_file_path could not allocate memory fur custom file path.");

            file_path = NULL;
            goto cleanup;
        }

        strncat(file_path, services_path->valuestring, strlen(services_path->valuestring));
    }
    else {
        char *dir_path = set_get_settings_dir_path();
        const char file_name[] = "/services.json";

        size_t path_size = strlen(dir_path) + strlen(file_name) + 1;

        file_path = (char *) malloc(path_size * sizeof(char));

        if(file_path == NULL) {
            fprintf(stderr, "Error: set_get_services_file_path could not allocate memory for default file path.");

            file_path = NULL;
            free(dir_path);
            goto cleanup;
        }

        strncpy(file_path, dir_path, path_size);
        strncat(file_path, file_name, path_size);

        free(dir_path);
    }

    cleanup:
        cJSON_Delete(settings);

        free(settings_file_path);
        free(data);

        return file_path;
}

char *set_read_file(const char *path) {
    if(path == NULL) {
        fprintf(stderr, "Error: set_read_file path cannot be NULL.\n");
        return NULL;
    }

    FILE *fp = fopen(path, "r");

    if(fp == NULL) {
        fprintf(stderr, "Error: set_read_file failed on reading %s.\n", path);
        return NULL;
    }

    fseek(fp, 0L, SEEK_END);
    size_t size = ftell(fp);
    fseek(fp, 0L, SEEK_SET);

    char *data = (char *) malloc((size + 1) * sizeof(char)); // +1 for string terminator

    if(fread(data, sizeof(char), size, fp) != size) {
        if(data != NULL) {
            free(data);
        }

        fclose(fp);

        fprintf(stderr, "Error: set_read failed on reading %s file.\n", path);
        return NULL;
    }
    // Last char must be set otherwise too otherwise 'Uninitialised value was created by a heap allocation' by valgrind
    data[size] = '\0';

    fclose(fp);

    return data;
}


int set_write_file(const char *path, const char *src) {
    FILE *file = fopen(path, "w");
    if(file == NULL) {
        fprintf(stderr, "Error: set_write_file failed on writing %s.\n", path);
        return -1;
    }

    fprintf(file, "%s", src);

    fclose(file);

    return 0;
}


cJSON *set_create_json(service s) {
    // Check if everything is ok
    if(s.name == NULL || s.password_length <= 0 || s.alphabet == NULL || s.created_on == NULL) {
        fprintf(stderr, "Fail: set_create_json failed on creating json from service struct.");
        return NULL;
    }

    // Create the json
    cJSON *obj = cJSON_CreateObject();
    cJSON *name = cJSON_CreateString(s.name);
    cJSON *password_length = cJSON_CreateNumber((double) s.password_length);
    cJSON *alphabet = cJSON_CreateString(s.alphabet);
    cJSON *created_on = cJSON_CreateString(s.created_on);

    cJSON_AddItemToObject(obj, "name", name);
    cJSON_AddItemToObject(obj, "password_length", password_length);
    cJSON_AddItemToObject(obj, "alphabet", alphabet);
    cJSON_AddItemToObject(obj, "created_on", created_on);

    return obj;
}

service set_create_service_from_json(cJSON *json) {
    service s;

    // set false if json is invalid
    s.ok = true;
    s.name = NULL;
    s.password_length = 0;
    s.alphabet = NULL;
    s.created_on = NULL;

    if(json == NULL) {
        fprintf(stderr, "Fail: set_create_service_from_json failed parsing json string:\n%s\n", cJSON_Print(json));
        s.ok = false;
    }

    cJSON *name = cJSON_GetObjectItem(json, "name");
    if(!cJSON_IsString(name) || (name->valuestring == NULL)) {
        fprintf(stderr, "Fail: set_create_service_from_json failed parsing name.");
        s.ok = false;
    } else {
        s.name = malloc((strlen(name->valuestring) + 1) * sizeof(char));
        // If the length of src is less than n,
        // strncpy() writes additional null bytes to dest to ensure that a total of n bytes are written.
        strncpy(s.name, name->valuestring, strlen(name->valuestring) + 1);
    }

    cJSON *password_length = cJSON_GetObjectItem(json, "password_length");
    if(!cJSON_IsNumber(password_length)) {
        fprintf(stderr, "Fail: set_create_service_from_json failed parsing password_length.");
        s.ok = false;
    } else {
        s.password_length = password_length->valueint;
    }

    cJSON *alphabet = cJSON_GetObjectItem(json, "alphabet");
    if(!cJSON_IsString(alphabet) || (alphabet->valuestring == NULL)) {
        fprintf(stderr, "Fail: set_create_service_from_json failed parsing alphabet.");
        s.ok = false;
    } else {
        s.alphabet = malloc((strlen(alphabet->valuestring) + 1) * sizeof(char));
        strncpy(s.alphabet, alphabet->valuestring, strlen(alphabet->valuestring) + 1);
    }

    cJSON *created_on = cJSON_GetObjectItem(json, "created_on");
    if(!cJSON_IsString(created_on) || (created_on->valuestring == NULL)) {
        fprintf(stderr, "Fail: set_create_service_from_json failed parsing created_on.");
        s.ok = false;
    } else {
        s.created_on = malloc((strlen(created_on->valuestring) + 1) * sizeof(char));
        strncpy(s.created_on, created_on->valuestring, strlen(created_on->valuestring) + 1);
    }

    s.ok = true;

    return s;
}

size_t set_read_services(service **services_ptr) {
    char *services_file_path = set_get_services_file_path();
    char *json_str = set_read_file(services_file_path);

    cJSON *obj = cJSON_Parse(json_str);
    cJSON *ary = cJSON_GetObjectItemCaseSensitive(obj, "services");

    size_t ary_size = cJSON_GetArraySize(ary);

    *services_ptr = (service *) malloc(ary_size * sizeof(service));

    if(*services_ptr == NULL) {
        fprintf(stderr, "Error: set_read_services could not allocate memory.\n");
        ary_size = 0;
        goto cleanup;
    }

    for(int i=0; i<ary_size; ++i) {
        (*services_ptr)[i] = set_create_service_from_json(cJSON_GetArrayItem(ary, i));
    }

    cleanup:
        cJSON_Delete(obj);
        free(services_file_path);
        free(json_str);

    return ary_size;
}

int set_write_services(service *ary, size_t count) {
    int return_value = 0;
    cJSON *json_ary = cJSON_CreateArray();

    for(int i = 0; i < count; ++i) {
        cJSON *json = set_create_json(ary[i]);
        cJSON_AddItemToArray(json_ary, json);
    }

    cJSON *json_obj = cJSON_CreateObject();
    cJSON_AddItemToObject(json_obj, "services", json_ary);

    char *json_print = cJSON_Print(json_obj);
    char *services_file_path = set_get_services_file_path();
    if(set_write_file(services_file_path, json_print) != 0) {
        fprintf(stderr, "Fail: set_write_services failed to write.");

        return_value = -1;
        goto cleanup;
    }

    cleanup:
        cJSON_Delete(json_obj);
        free(json_print);
        free(services_file_path);

    return return_value;
}

int set_add_new_service(service new) {
    service *services_ptr = NULL;

    size_t services_count = set_read_services(&services_ptr);
    services_ptr = (service *) realloc(services_ptr, (services_count + 1) * sizeof(service));
    services_ptr[services_count] = new;

    int return_value = set_write_services(services_ptr, services_count + 1);

    set_free_services(services_ptr, services_count);

    return return_value;
}

service *set_free_services(service *services_ptr, size_t count) {
    for(int i=0; i<count; ++i) {
        set_free_single_service(services_ptr+i);
    }

    free(services_ptr);

    return services_ptr;
}

void set_free_single_service(service *service_ptr) {
    if(service_ptr->name != NULL)
        free(service_ptr->name);

    if(service_ptr->alphabet != NULL)
        free(service_ptr->alphabet);

    if(service_ptr->created_on != NULL)
        free(service_ptr->created_on);

    service_ptr->ok = false;
}