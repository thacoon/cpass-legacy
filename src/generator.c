#include "generator.h"

const char *ALPHABET_SIMPLE = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

int gen_init() {
    if(cryp_init() == 0) {
        return 0;
    }

    return -1;
}

char *gen_sha512(const char *password) {
    unsigned char *hash_hex = cryp_sha512(password);

    if(hash_hex == NULL) {
        fprintf(stderr, "gen_sha512: Failed to generate hash.\n");
    }

    char *hash = cryp_convert_sha512_hex_to_char(hash_hex);

    return hash;
}

void gen_password(char *hash_ptr, char *password_ptr) {
    mpz_t num, num_idx;
    int flag_num;

    mpz_init(num); mpz_init(num_idx);
    mpz_set_ui(num, 0); mpz_set_ui(num_idx, 0);

    flag_num = mpz_set_str(num, hash_ptr, 16);
    if(flag_num != 0) {
        fprintf(stderr, "Error: get_password failed on init num.\n");
    }

    int i;
    int num_chars = strlen(ALPHABET_SIMPLE);
    unsigned long int idx;
    for(i = 0; i < PASSWORD_LENGTH; i++) {
        mpz_mod_ui(num_idx, num, num_chars);
        mpz_div_ui(num, num, num_chars);
        idx = mpz_get_ui(num_idx);

        password_ptr[i] = ALPHABET_SIMPLE[idx];
    }
    password_ptr[i] = '\0';

    mpz_clear(num);
    mpz_clear(num_idx);
}

char *gen_generate_password(char *passphrase) {
    char *hash_ptr = gen_sha512(passphrase);

    char *password = sodium_allocarray(PASSWORD_LENGTH_WITH_NULLBYTE, sizeof(char));
    gen_password(hash_ptr, password);

    sodium_memzero(hash_ptr, HASH_SHA512_LEN);
    sodium_free(hash_ptr);

    return password;
}
