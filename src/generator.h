#ifndef GENERATOR_H
#define GENERATOR_H

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>

#include <gmp.h>
#include <sodium.h>

#include "cryp.h"

#define PASSWORD_LENGTH 20
#define PASSWORD_LENGTH_WITH_NULLBYTE (20 + 1)

/**
* @brief Init the generator. Mainly used to init libsodium lib.
* @return 0 on success otherwise -1.
*/
int gen_init(void);

/**
* @brief Generates a sha512 hash.
* @param password is the string to compute the hash from.
* @return NULL if no memory could be allocated otherwise hash_ptr.
*/
char *gen_sha512(const char *password);
/**
* @brief Generates a password of length 15.
* @param hash_ptr is the hash to use for password generation.
* @param password_ptr stores the password.
*/
void gen_password(char *hash_ptr, char *password_ptr);
/**
* @brief Handles the generation of a password.
* @param passphrase the passphrase/password to generate the password from.
* @param password stores the generated password.
*/
char *gen_generate_password(char *passphrase);


#endif
