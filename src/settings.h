#ifndef CPASS_SETTINGS_H
#define CPASS_SETTINGS_H

#define _GNU_SOURCE

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "lib/cJSON.h"


/**
 * @struct A struct that holds information for services
 * @var name is the service name attached to the master password.
 * @var password_length is the length of the generated password.
 * @var alphabet is the alphabet that should be used for password creation. Currently available are: simple
 * @var created_on is the date of the creation in this format 2018-05-01
 * @var ok is true if the service is valid otherwise false.
 */
struct service {
    char *name;
    size_t password_length;
    char *alphabet;
    char *created_on;
    bool ok;
};
typedef struct service service;

/**
 * @brief Initializes the settings file.
 * @details Create a default settings file if none exists.
 */
void set_init();

/**
 * Get the absolute path fo the dir where the cpass settings are stored.
 * @return the path of the dir. Needs to be freed.
 */
char *set_get_settings_dir_path(void);

/**
 * Get the absolute path fo the settings file where the cpass settings are stored.
 * @return the path of the file. Needs to be freed.
 */
char *set_get_settings_file_path(void);

/**
 * Get the absolute path fo the service file where the cpass services are stored.
 * @return the path of the file. Needs to be freed.
 */
char *set_get_services_file_path(void);

/**
 * @brief Read settings file from disk.
 * @param path is the path to file.
 * @return a ptr to allocated memory that contains the file data otherwise NULL on error.
 */
char *set_read_file(const char *path);

/**
 * @brief Write settings json string data to a file.
 * * @param path is the path to file.
 * @param src is a ptr to allocated memory that contains data that should be written to the file.
 * @return 0 on success otherwise -1.
 */
int set_write_file(const char *path, const char *src);

/**
 * @brief Create a json string from a service struct.
 * @param s is service struct.
 * @return a cJSON ptr of the json and NULL on error.
 */
cJSON *set_create_json(service s);

/**
 * @brief Reads a json string and convert it to a service struct.
 * @param json is cJSON data.
 * @return a service struct based on the information from the given json string on error a service struct with s.ok=false.
 */
service set_create_service_from_json(cJSON *json);

// @TODO Test these functions, currently cannot mock set_get_dir_path, maybe use variable settings path
// Would need different obj files that are linked in production or #ifdefs, see:
// https://github.com/meekrosoft/fff/issues/36

/**
 * @brief Read the settings file and store all services in an array.
 * @param services_ptr is a pointer to a pointer used to allocated memory for the service array.
 * @return the number of services and 0 of error.
 */
size_t set_read_services(service **services_ptr);

/**
 * @brief Write all services to the settings json file.
 * @param ary is a pointer to a service array.
 * @param count is the number of services in the array.
 * @return 0 on success otherwise -1.
 */
int set_write_services(service *ary, size_t count);

/**
 * Add a new service to the settings file.
 * @param new is the service to add to the settings file.
 * @return 0 on success otherwise -1.
 */
int set_add_new_service(service new);

/**
 * @brief Free allocated memory from every service in a service struct and also the whole array.
 * @param services_ptr is a ptr to a service array.
 * @param count is the number of services in the array.
 * @return NULL if successful freed.
 */
service *set_free_services(service *services_ptr, size_t count);

/**
 * @brief Free allocated memory from a single service struct.
 */
void set_free_single_service(service *service_ptr);

#endif //CPASS_SETTINGS_H
