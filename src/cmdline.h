#ifndef CMDLINE_H
#define CMDLINE_H

#include <math.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <termios.h>
#include <time.h>

#include "cryp.h"
#include "generator.h"
#include "settings.h"


/**
* @brief Init the generator. Mainly used to init libsodium lib.
* @return 0 on success otherwise -1.
*/
int cmd_init(void);

/**
* @brief Ask the user for the master password allocate memory and saves it.
* @details Disables echoing. The memory is allocated via libsodium secure memory functions.
* @return pointer to allocated memory.
*/
char *cmd_ask_user_for_password(void);

/**
 * Print all available services to stdin.
 */
void cmd_print_all_services(void);

/**
* @brief Ask the user for the service and saves it.
* @param buffer is a ptr to store the service.
* @param buffer_size is the max size of the buffer.
*/
void cmd_ask_user_for_service(char *buffer, int buffer_size);

/*
 * @brief Save a new service in the settigns file.
 */
void cmd_save_new_service(char *name);

/**
* @brief Generates the passphrase and print it to the screen.
* @param password is the master password.
* @param service is the service for that the passprhase should be generated.
*/
void cmd_print_passphrase(char *password, char *service_name);

/**
* @brief Compares if two passwords are equal.
* @param password_one is the first password to compare.
* @param password_two is the second password to compare.
* @return 0 if passwords are equal otherwise -1.
*/
int cmd_compare_passwords(const char *password_one, const char *password_two);

/**
 * @param service_ary
 * @param count is the number of services in service_ary
 * @return the length of the longest service name
 */
size_t cmd_get_longest_service_name(service *service_ary, size_t count);

/**
 * @brief Flush stdin
 */
 void cmd_flush_stdin(void);

#endif
