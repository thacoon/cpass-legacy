#include "cmdline.h"

int cmd_init() {
    if(cryp_init() == 0) {
        return 0;
    }

    return -1;
}

char *cmd_ask_user_for_password(void) {
    struct termios old_termios, new_termios;

    int password_length = 512;
    char *password = (char *) sodium_allocarray(password_length, sizeof(char));
    sodium_memzero(password, password_length);

    printf("%s", "Enter your master password: ");

    if(tcgetattr(fileno(stdin), &old_termios) != 0) {
        return NULL;
    }

    new_termios = old_termios;
    new_termios.c_lflag &= ~ECHO;

    // disable echo to terminal
    if(tcsetattr(fileno(stdin), TCSAFLUSH, &new_termios) != 0) {
        return NULL;
    }

    if(fgets(password, password_length, stdin) == NULL) {
        fprintf(stderr, "Error: cmd_ask_user_for_password could not read password.");
        *password = '\0';
    }
    else {
        // terminate the password just to be sure
        password[password_length - 1] = '\0';

        // remove newline from string
        size_t actual_length = strlen(password);
        if(password[actual_length - 1] == '\n') {
            password[actual_length - 1] = '\0';
            --actual_length;
        }

        if(strlen(password) >= (password_length-1)) {
            fprintf(stderr, "Error: cmd_ask_user_for_password password to long. Max length is %d.", password_length - 1);

            cmd_flush_stdin();
            goto cleanup;
        }
    }

    cleanup:
        // reenable echo to terminal
        (void) tcsetattr(fileno(stdin), TCSAFLUSH, &old_termios);
        printf("\n");

    return password;
}

void cmd_print_all_services(void) {
    service *service_ary;

    size_t count = set_read_services(&service_ary);

    size_t longest_service_name = cmd_get_longest_service_name(service_ary, count);

    // set a dynamic row width based on the longest string in each row
    int headline_length = 0;
    printf("%*s | %*s | %*s | %*s | %*s %n\n",
           (int) (log(count) > 2 ? log(count) : 2), "id",
           (int) longest_service_name, "name",
           (int) strlen("password length"), "password length",
           (int) strlen("alphabet"), "alphabet",
           (int) strlen("created on"), "created on",
            &headline_length);

    for(int i=0; i<headline_length; ++i) {
        printf("-");
    }
    printf("\n");

    for(int i=0; i<count; ++i) {
        printf("%*d | %*s | %*lu | %*s | %*s\n",
               (int) (log(count) > 2 ? log(count) : 2), i,
               (int) longest_service_name, service_ary[i].name,
               (int) strlen("password length"), service_ary[i].password_length,
               (int) strlen("alphabet"), service_ary[i].alphabet,
               (int) strlen("created on"), service_ary[i].created_on);
    }

    set_free_services(service_ary, count);

    return;
}

void cmd_ask_user_for_service(char *buffer, int buffer_size) {
    printf("Enter the service: ");
    fgets(buffer, buffer_size, stdin);

    char *ptr = buffer;
    int i = 0;

    // remove newline from buffer
    while((*ptr != '\0') && (*ptr != EOF) && (i < buffer_size)) {
        if(*ptr == '\n') {
            *ptr = '\0';
            return;
        }
        ptr++; i++;
    }
    *ptr = '\0';

    if(strlen(buffer) >= (buffer_size-1)) {
        fprintf(stderr, "Error: cmd_ask_user_for_service service name too long. Max length is %d.", buffer_size-1);
        cmd_flush_stdin();
    }

    printf("\n");
}

void cmd_save_new_service(char *name) {
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);

    char tm_buffer[12];
    snprintf(tm_buffer, sizeof(tm_buffer), "%04d-%02d-%02d", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday);

    service new;
    new.name = name;
    new.password_length = 20;
    new.alphabet = "simple";
    new.created_on = tm_buffer;
    new.ok = true;

    set_add_new_service(new);

    return;
}

void cmd_print_passphrase(char *passphrase, char *service_name) {
    size_t combined_passphrase_length = strlen(passphrase) + strlen(service_name);
    char *combined_passphrase = sodium_allocarray(combined_passphrase_length + 1, sizeof(char));

    if(combined_passphrase == NULL) {
        fprintf(stderr, "Error: cmd_print_password could not allocate memory.");
        goto cleanup;
    }

    strncpy(combined_passphrase, passphrase, combined_passphrase_length);
    strncat(combined_passphrase, service_name, combined_passphrase_length);

    char *password = gen_generate_password(combined_passphrase);

    printf("Password: %s\n", password);

    sodium_memzero(password, PASSWORD_LENGTH_WITH_NULLBYTE);
    sodium_free(password);

    cleanup:
        sodium_memzero(combined_passphrase, combined_passphrase_length + 1);
        sodium_free(combined_passphrase);
}

int cmd_compare_passwords(const char *password_one, const char *password_two) {
    if((password_one == NULL) || (password_two == NULL)) {
        return -1;
    }

    if(strlen(password_one) != strlen(password_two)) {
        return -1;
    }

    if(strcmp(password_one, password_two) == 0) {
        return 0;
    }
    else {
        return -1;
    }
}

size_t cmd_get_longest_service_name(service *service_ary, size_t count) {
    size_t longest_service_name = 0;

    for(int i=0; i < count; ++i) {
        size_t len = strlen(service_ary[i].name);

        if (len > longest_service_name) {
            longest_service_name = len;
        }
    }

    return longest_service_name;
}

void cmd_flush_stdin(void) {
    char c;
    while ((c = getchar()) != '\n' && c != EOF) {}
}