#ifndef CRYP_H
#define CRYP_H

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <gmp.h>
#include <sodium.h>

#define HASH_SHA512_LEN (crypto_hash_sha512_BYTES * 2 )
#define HASH_SHA512_LEN_WITH_NULLBYTE (crypto_hash_sha512_BYTES * 2 + 1)

#if defined(__linux__)
    #include <fcntl.h>
    #include <unistd.h>
    #include <stdlib.h>
    #include <sys/ioctl.h>
    #include <linux/random.h>
#endif

#if defined(__linux__)
    /**
    * @brief Checks on linux systems if it has enough entropy. If not abort the program.
    * @details '# cat /proc/sys/kernel/random/entropy_avail' should be greater than 160.
    */
    void cryp_check_entropy(void);
#endif

/**
* @brief Init the libsodium library for encryption/decryption.
* @details It would be safe to call sodium_init() multiple times but currently using assert to check wheter function is called multiple times. Doing it to prevent possible logical flaws.
* @return 0 on success otherwise -1.
*/
int cryp_init(void);

/**
* @details Generates a sha512 hash.
* @param str is the string that should be used to generate the hash.
* @return the sha512 hash of the password.
*/
unsigned char *cryp_sha512(const char *str);

/**
* @details Libsodium generates a hex hash and this function convert it into a normal char string.
* @param hash is a sha512 hash in hex representation.
* @return an allocated hash char string.
*/
char *cryp_convert_sha512_hex_to_char(const unsigned char *hash);

/**
* @brief Generates a passphrase that can be used in login forms and so on based on a sha512 hash.
* @details TODO: a short explanation of the basic principle.
* @param hash is a sha512 hash that is used to generate the passphrase.
* @param passphrase_len is the length of the passphrase that should be generated.
* @return an allocated string of the generated passphrase.
*/
char *cryp_derive_passphrase(unsigned char *hash, size_t passphrase_len);
#endif
